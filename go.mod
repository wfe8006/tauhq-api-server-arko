module tauhq.com/tauhq-api-server-arko

go 1.15

require (
	github.com/cockroachdb/apd v1.1.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.4.0
	github.com/lib/pq v1.10.5
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rs/cors v1.8.2
)
