package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/cockroachdb/apd"
	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/rs/cors"
)

type BlockDB struct {
	Hash         string
	Timestamp    int
	ID           int
	Txn          int
	Transactions []string        `json:",omitempty"`
	LatestBlock  int             `json:",omitempty"`
	Proofs       json.RawMessage `json:",omitempty"`
	Origin       json.RawMessage `json:",omitempty"`
	Rewards      json.RawMessage `json:",omitempty"`
	Number       string
}

type TransactionDB struct {
	Signature       string `json:",omitempty"`
	Hash            string
	Sender          string `json:",omitempty"`
	Recipient       string `json:",omitempty"`
	Processor       string `json:",omitempty"`
	Timestamp       int
	ID              int
	BlockID         int             `json:",omitempty"`
	StampsSupplied  int             `json:",omitempty"`
	Nonce           int             `json:",omitempty"`
	StampsUsed      int             `json:",omitempty"`
	SubBlockNO      int             `json:",omitempty"`
	HasError        bool            `json:",omitempty"`
	Result          string          `json:",omitempty"`
	Contract        string          `json:",omitempty"`
	Function        string          `json:",omitempty"`
	Kwargs          json.RawMessage `json:",omitempty"`
	State           json.RawMessage `json:",omitempty"`
	KwargsAmount    *apd.Decimal    `json:",omitempty"`
	Amount          string
	Flow            string `json:",omitempty"`
	OK              int
	SwapRateTAU     string `json:",omitempty"`
	Action          string `json:",omitempty"`
	TAUPriceUSD     string `json:",omitempty"`
	StampsPerTAU    int    `json:",omitempty"`
	StampsBurnRatio string `json:",omitempty"`
}

type State struct {
	Key   string          `json:"Key"`
	Value json.RawMessage `json:"value"`
}

type AddressDB struct {
	Address    string
	Txn        int    `json:",omitempty"`
	ID         uint64 `json:",omitempty"`
	Balance    string
	BalanceUSD string `json:",omitempty"`
}

type TokenAddressDB struct {
	Addresses   []AddressDB
	TotalSupply string
}

type TokenDB struct {
	ID                uint64 `json:",omitempty"`
	TokenContract     string `json:",omitempty"`
	TokenName         string `json:",omitempty"`
	TokenSymbol       string `json:",omitempty"`
	TotalSupply       uint64 `json:",omitempty"`
	CirculatingSupply string `json:",omitempty"`
	MarketcapTAU      int    `json:",omitempty"`
	LastPriceTAU      float64
	Txn               int
	TokenHolder       int
	TAUPriceUSD       string          `json:",omitempty"`
	TokenBalance      string          `json:",omitempty"`
	TokenBurned       float64         `json:",omitempty"`
	Social            json.RawMessage `json:",omitempty"`
}

type DappDB struct {
	Website     string `json:",omitempty"`
	Logo        string `json:",omitempty"`
	Name        string `json:",omitempty"`
	DappID      int    `json:",omitempty"`
	Category    string `json:",omitempty"`
	Description string `json:",omitempty"`
}

type ContractDB struct {
	ContractName    string
	DailyStampsUsed int    `json:",omitempty"`
	DailyTxn        int    `json:",omitempty"`
	DailyAddresses  int    `json:",omitempty"`
	TotalStampsUsed int    `json:",omitempty"`
	TotalTxn        int    `json:",omitempty"`
	TotalAddresses  int    `json:",omitempty"`
	DappName        string `json:",omitempty"`
	Logo            string `json:",omitempty"`
	Website         string `json:",omitempty"`
	ContractCode    string `json:",omitempty"`
	ContractCreator string `json:",omitempty"`
	ContractCreated int    `json:",omitempty"`
	ByteCode        string `json:",omitempty"`
	TXHash          string `json:",omitempty"`
	//ChartData       []ChartGroupDateValue `json:",omitempty"`
	ChartData       MainChart `json:",omitempty"`
	DappID          int       `json:",omitempty"`
	DappDescription string    `json:",omitempty"`
	DappCategory    string    `json:",omitempty"`
}

type MainChart struct {
	XData    []int
	Datasets []SubChart
}

type SubChart struct {
	Name    string
	DataF   []float32 `json:",omitempty"`
	DataI   []int     `json:",omitempty"`
	Unit    string
	Type    string
	Decimal int
	Axis    string `json:",omitempty"`
	Prefix  string `json:",omitempty"`
	Suffix  string `json:",omitempty"`
}

type TokenChartDB struct {
	ChartData MainChart `json:",omitempty"`
}

type ContractUsage struct {
	Timestamp int
	Value     int
}

type ContractSimpleDB struct {
	ContractName string
	ID           int
}

type TAUTicker struct {
	//PriceUSD string `json:"price_usd"`
	Value string `json:"value"`
}

type CoinPaprikaWethTicker struct {
	PriceUSD string `json:"price_usd"`
}

type Summary struct {
	TotalBlock int
	TotalTxn   int
	//TotalContract int
	TotalAddress  int
	TotalContract int
	TotalToken    int
	TotalDapp     int
	TotalBurned   string
	TotalTAU      float64
	TAUPrice      string
	StampsPerTAU  string
	Contracts     []ContractDB
}

type AddressTransactionDB struct {
	Transactions    []TransactionDB
	LatestID        int
	OldestID        int
	Txn             int          `json:",omitempty"`
	Balance         *apd.Decimal `json:",omitempty"`
	BalanceUSD      *apd.Decimal `json:",omitempty"`
	Symbol          string       `json:",omitempty"`
	TokenBalance    string       `json:",omitempty"`
	TokenBalanceUSD string       `json:",omitempty"`
	TokenSymbol     string       `json:",omitempty"`
	TotalSupply     string       `json:",omitempty"`
	Tokens          []TokenDB    `json:",omitempty"`
}

type ChartGroupDateValue struct {
	Name       string
	Data       [][2]int    `json:",omitempty"`
	DataString [][2]string `json:",omitempty"`
}

type ChartSingleDateValue struct {
	SingleDateValue [2]int
}

type SearchResult struct {
	Type string
}

type TAULost struct {
	Address string
	Balance string
}

var db *sql.DB
var redisClient *redis.Client
var latestBlock int
var latestTransaction int
var latestContract int
var tauPiceLastUpdate = 0

//var tauPriceUSD string
var tauPriceUSD = ""

func sanitizeURL(id string) string {
	id = strings.TrimSpace(id)
	// Make a Regex to say we only want letters and numbers, hyphen, underscore and space
	reg, err := regexp.Compile("[^a-zA-Z0-9-_ ]+")
	if err != nil {
		fmt.Printf("line 225\n")
	}
	processedString := reg.ReplaceAllString(id, "")
	fmt.Printf("A string of %s becomes %s \n", id, processedString)
	return processedString
}

func checkTAUPrice1() {
	t := time.Now().UTC()
	yyyymmdd := t.Format("20060102")
	err := redisClient.Do("HSET", "h_tau_daily_usd_price:"+yyyymmdd, "price", tauPriceUSD).Err()
	if err != nil {
		//sentry.CaptureException(err)
		fmt.Printf("line 265\n")
	}
}

func checkTAUPrice() {
	//fmt.Printf("Checking TAU Price...")
	now := int(time.Now().Unix())
	if now-tauPiceLastUpdate > 3600 {
		//res, err := http.Get("https://api.coinpaprika.com/v1/ticker/tau-lamden")
		res, err := http.Get("https://rocketswap.exchange/api/tau_last_price")
		if err != nil {
			fmt.Printf("line 238\n")
		}
		body, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			fmt.Printf("line 243\n")
		}
		if res.StatusCode != 200 {
			fmt.Printf("line 246 %d\n", res.StatusCode)
		}
		var tauTicker TAUTicker
		err = json.Unmarshal(body, &tauTicker)
		if err != nil {
			fmt.Printf("line 251\n")
		}
		tauPriceUSD = tauTicker.Value
		tauPiceLastUpdate = now
		//fmt.Printf("updated TAU price\n")
	} else {
		//fmt.Printf("no update needed\n")
	}

	t := time.Now().UTC()
	yyyymmdd := t.Format("20060102")
	err := redisClient.Do("HSET", "h_tau_daily_usd_price:"+yyyymmdd, "price", tauPriceUSD).Err()
	if err != nil {
		//sentry.CaptureException(err)
		fmt.Printf("line 265\n")
	}
	fmt.Printf("date price: %s\n", "h_tau_daily_usd_price:"+yyyymmdd)

}

func getSummary(w http.ResponseWriter, r *http.Request) {

	checkTAUPrice()

	latestBlockID, err := redisClient.Get("latest_block_id").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}
	latestBlockIDInt, err := strconv.Atoi(latestBlockID)
	latestBlock := latestBlockIDInt

	latestTransactionID, err := redisClient.Get("latest_transaction_id").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}
	latestTransactionIDInt, err := strconv.Atoi(latestTransactionID)
	latestTransaction := latestTransactionIDInt

	totalAddressCountStr, err := redisClient.Get("total_addresses").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}
	totalAddressCountInt, err := strconv.Atoi(totalAddressCountStr)
	totalAddressCount := totalAddressCountInt

	latestContractID, err := redisClient.Get("latest_contract_id").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}
	latestContractIDInt, err := strconv.Atoi(latestContractID)
	latestContract := latestContractIDInt

	totalTokenCountStr, err := redisClient.Get("total_tokens").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}
	totalTokenCountInt, err := strconv.Atoi(totalTokenCountStr)
	totalTokenCount := totalTokenCountInt

	totalDappCountStr, err := redisClient.Get("total_dapps").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}
	totalDappCountInt, err := strconv.Atoi(totalDappCountStr)
	totalDappCount := totalDappCountInt

	totalBurned, err := redisClient.Get("total_burned").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}

	totalTAULostCountStr, err := redisClient.Get("total_tau_lost").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}

	totalBurnedFloat, err := strconv.ParseFloat(totalBurned, 64)
	if err != nil {
		fmt.Printf("line 1145\n")
	}
	totalTAULostCountFloat, err := strconv.ParseFloat(totalTAULostCountStr, 64)
	totalTAU := 288000000 - totalTAULostCountFloat - totalBurnedFloat

	stampsPerTAU, err := redisClient.Get("stamps_per_tau").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}

	contractArr := []string{
		"con_dice001",
		"con_hopium",
		"con_lamden_link_v1",
		"con_pixel_whale_master_v1",
		"con_puzzles_v1",
		"con_random_allocator",
		"con_reflecttau_v2",
		"con_rocketswap_official_v1_1",
		"con_smack_that_6",
		"submission"}

	rand.Shuffle(len(contractArr), func(i, j int) {
		contractArr[i], contractArr[j] = contractArr[j], contractArr[i]
	})
	var contracts []ContractDB
	for i := 0; i < 4; i++ {
		dappName, err := redisClient.HGet(contractArr[i], "name").Result()
		if err != nil {
			fmt.Printf("line 340\n")
		}
		logo, err := redisClient.HGet(contractArr[i], "logo").Result()
		if err != nil {
			fmt.Printf("line 344\n")
		}
		category, err := redisClient.HGet(contractArr[i], "category").Result()
		if err != nil {
			fmt.Printf("line 348\n")
		}
		description, err := redisClient.HGet(contractArr[i], "description").Result()
		if err != nil {
			fmt.Printf("line 352\n")
		}

		contract := ContractDB{
			DappName:        dappName,
			Logo:            logo,
			DappCategory:    category,
			DappDescription: description,
		}
		contracts = append(contracts, contract)
	}

	sort.Slice(contracts, func(i, j int) bool {
		return contracts[i].DailyTxn > contracts[j].DailyTxn
	})

	summary := Summary{
		TotalBlock:    latestBlock,
		TotalTxn:      latestTransaction,
		TotalAddress:  totalAddressCount,
		TotalContract: latestContract,
		TotalToken:    totalTokenCount,
		TotalDapp:     totalDappCount,
		TotalBurned:   totalBurned,
		TotalTAU:      totalTAU,
		TAUPrice:      tauPriceUSD,
		StampsPerTAU:  stampsPerTAU,
		Contracts:     contracts,
	}

	json.NewEncoder(w).Encode(summary)
}

func getBlocks(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	latestBlockID, err := redisClient.Get("latest_block_id").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}
	latestBlockIDInt, err := strconv.Atoi(latestBlockID)
	latestBlock = latestBlockIDInt
	page, err := strconv.Atoi(r.URL.Query().Get("page"))

	if err != nil || page < 2 {
		page = 1
	}
	var min = latestBlock
	if page > 1 {
		min = latestBlock - ((page - 1) * 25)
	}
	sqlStatement := `SELECT hash, COALESCE(timestamp, 0), id FROM blocks WHERE id <= $1 ORDER BY id DESC LIMIT 25`
	rows, err := db.Query(sqlStatement, min)
	if err != nil {
		fmt.Printf("line 402\n")
	}
	var blocks []BlockDB
	for rows.Next() {
		var hash string
		var timestamp int
		var id int

		err = rows.Scan(&hash, &timestamp, &id)
		block := BlockDB{
			Hash:      hash,
			Timestamp: timestamp,
			ID:        id,
		}
		blocks = append(blocks, block)
		if err != nil {
			fmt.Printf("line 421\n")
		}
	}
	json.NewEncoder(w).Encode(blocks)
}

func getBlock(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	vars := mux.Vars(r)
	id, err := strconv.Atoi(vars["id"])
	if err != nil {
		id = 1
	}
	var block BlockDB
	if id == 0 {
		//if id == 70704 || id == 501400 || id == 501401 || id == 507274 || id == 507275 || id == 507992 {
		block = BlockDB{
			ID:   id,
			Hash: "notfound",
		}
	} else {
		sqlStatement := `SELECT hash FROM transactions WHERE block_id = $1`
		rows, err := db.Query(sqlStatement, id)
		if err != nil {
			fmt.Printf("line 444\n")
		}
		var transactions []string
		for rows.Next() {
			var hash string
			err = rows.Scan(&hash)
			transactions = append(transactions, hash)
			if err != nil {
				fmt.Printf("line 452\n")
			}
		}

		sqlStatement = `SELECT hash, timestamp, id, proofs, origin, rewards, number, txn FROM blocks WHERE id = $1`
		rows, err = db.Query(sqlStatement, id)
		if err != nil {
			fmt.Printf("line 459\n")
		}
		for rows.Next() {
			var hash string
			var timestamp int
			var id int
			var proofs string
			var origin string
			var rewards string
			var number string
			var txn int

			var err = rows.Scan(&hash, &timestamp, &id, &proofs, &origin, &rewards, &number, &txn)
			block = BlockDB{
				Hash:         hash,
				Timestamp:    timestamp,
				ID:           id,
				Transactions: transactions,
				Proofs:       json.RawMessage(proofs),
				Origin:       json.RawMessage(origin),
				Rewards:      json.RawMessage(rewards),
				Number:       number,
				Txn:          txn,
				LatestBlock:  latestBlock,
			}
			if err != nil {
				fmt.Printf("line 478\n")
			}
		}

	}

	json.NewEncoder(w).Encode(block)
}

func getTransactions(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	latestTransactionID, err := redisClient.Get("latest_transaction_id").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}
	latestTransactionIDInt, err := strconv.Atoi(latestTransactionID)
	latestTransaction = latestTransactionIDInt

	page, err := strconv.Atoi(r.URL.Query().Get("page"))
	if err != nil || page < 2 {
		page = 1
	}
	var min = latestTransaction
	if page > 1 {
		min = latestTransaction - ((page - 1) * 25)
	}
	sqlStatement := `SELECT hash, timestamp, id, contract, function, stamps_used, kwargs_amount, has_error, stamps_per_tau, stamps_burn_ratio FROM transactions WHERE id <= $1 ORDER BY block_id DESC LIMIT 25`
	rows, err := db.Query(sqlStatement, min)
	if err != nil {
		fmt.Printf("line 507\n")
	}
	var transactions []TransactionDB
	for rows.Next() {
		var hash string
		var timestamp int
		var id int
		var contract string
		var function string
		var stampsUsed int
		var hasError bool
		var ok int
		var stampsPerTAU int
		var stampsBurnRatio string
		kwargsAmount := new(apd.Decimal)
		err = rows.Scan(&hash, &timestamp, &id, &contract, &function, &stampsUsed, &kwargsAmount, &hasError, &stampsPerTAU, &stampsBurnRatio)
		if hasError {
			ok = 0
		} else {
			ok = 1
		}

		transaction := TransactionDB{
			Hash:            hash,
			Timestamp:       timestamp,
			ID:              id,
			Contract:        contract,
			Function:        function,
			StampsUsed:      stampsUsed,
			KwargsAmount:    kwargsAmount,
			OK:              ok,
			StampsPerTAU:    stampsPerTAU,
			StampsBurnRatio: stampsBurnRatio,
		}
		transactions = append(transactions, transaction)
		if err != nil {
			fmt.Printf("line 539\n")
		}
	}
	json.NewEncoder(w).Encode(transactions)
}

func getTransaction(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	vars := mux.Vars(r)
	id := sanitizeURL(vars["id"])

	sqlStatement := `SELECT signature, hash, sender, processor, timestamp, id, block_id, stamps_supplied, nonce, stamps_used,subblock_no, has_error, result, contract, function, kwargs, state, kwargs_amount, COALESCE(tau_price_usd, '-') AS tau_price_usd,  stamps_per_tau, stamps_burn_ratio FROM transactions WHERE hash = $1`
	rows, err := db.Query(sqlStatement, id)
	if err != nil {
		fmt.Printf("line 553\n")
	}
	var transaction TransactionDB
	for rows.Next() {
		var signature string
		var hash string
		var sender string
		var processor string
		var timestamp int
		var id int
		var blockID int
		var stampsSupplied int
		var nonce int
		var stampsUsed int
		var subBlockNO int
		var hasError bool
		var result string
		var contract string
		var function string
		var kwargs string
		var state string
		var tauPriceUSD string
		var stampsPerTAU int
		var stampsBurnRatio string

		kwargsAmount := new(apd.Decimal)
		err = rows.Scan(&signature, &hash, &sender, &processor, &timestamp, &id, &blockID, &stampsSupplied, &nonce, &stampsUsed, &subBlockNO, &hasError, &result, &contract, &function, &kwargs, &state, &kwargsAmount, &tauPriceUSD, &stampsPerTAU, &stampsBurnRatio)
		transaction = TransactionDB{
			Signature:       signature,
			Hash:            hash,
			Sender:          sender,
			Processor:       processor,
			Timestamp:       timestamp,
			ID:              id,
			BlockID:         blockID,
			StampsSupplied:  stampsSupplied,
			Nonce:           nonce,
			StampsUsed:      stampsUsed,
			SubBlockNO:      subBlockNO,
			HasError:        hasError,
			Result:          result,
			Contract:        contract,
			Function:        function,
			Kwargs:          json.RawMessage(kwargs),
			State:           json.RawMessage(state),
			KwargsAmount:    kwargsAmount,
			TAUPriceUSD:     tauPriceUSD,
			StampsPerTAU:    stampsPerTAU,
			StampsBurnRatio: stampsBurnRatio,
		}
		if err != nil {
			fmt.Printf("line 599\n")
		}
	}
	json.NewEncoder(w).Encode(transaction)
}

func getAddresses(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	checkTAUPrice()

	//var greaterOrLesser string = "<"
	dir := r.URL.Query().Get("dir")
	if dir != "prev" && dir != "next" {
		dir = "next"
		//greaterOrLesser = "<"
	}
	var lastID int
	var lastBalance float64

	if len(r.URL.Query().Get("id")) > 0 {
		var err error
		lastID, err = strconv.Atoi(r.URL.Query().Get("id"))
		if err != nil {
			lastID = 2147483646
		}
	} else {
		lastID = 2147483646
	}
	if len(r.URL.Query().Get("balance")) > 0 {
		var err error
		lastBalance, err = strconv.ParseFloat(r.URL.Query().Get("balance"), 64)
		if err != nil {
			fmt.Printf("line 631\n")
		}
	} else {
		lastBalance = 2147483646
	}

	var sqlStatement string
	if dir == "next" {
		sqlStatement = `SELECT address, txn, id, balance, ROUND(balance * $3, 2) AS balance_usd FROM top_addresses WHERE
	(balance, id) < ($1, $2) ORDER BY balance DESC, id DESC LIMIT 25`
	} else {
		sqlStatement = `SELECT address, txn, id, balance, balance_usd FROM(SELECT address, txn, id, balance, ROUND(balance * $3, 2) AS balance_usd FROM top_addresses WHERE (balance, id) > ($1, $2) ORDER BY balance ASC, id ASC LIMIT 25) AS prev_page ORDER BY balance DESC, id DESC `

	}
	rows, err := db.Query(sqlStatement, lastBalance, lastID, tauPriceUSD)
	if err != nil {
		fmt.Printf("line 648\n")
	}
	var addresses []AddressDB
	for rows.Next() {
		var address string
		var txn int
		var id uint64
		//balance := new(apd.Decimal)
		var balance string
		var balanceUSD string
		err = rows.Scan(&address, &txn, &id, &balance, &balanceUSD)
		if err != nil {
			fmt.Printf("line 659\n")
		}
		addressIndividual := AddressDB{
			Address:    address,
			Txn:        txn,
			ID:         id,
			Balance:    balance,
			BalanceUSD: balanceUSD,
		}
		addresses = append(addresses, addressIndividual)
	}
	json.NewEncoder(w).Encode(addresses)
}

func getAddress(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url::: %s\n", r.URL.RequestURI())
	checkTAUPrice()
	vars := mux.Vars(r)
	address := sanitizeURL(vars["address"])
	dir := r.URL.Query().Get("dir")
	if dir != "prev" && dir != "next" {
		dir = "next"
	}
	var lastID int
	if len(r.URL.Query().Get("id")) > 0 {
		var err error
		lastID, err = strconv.Atoi(r.URL.Query().Get("id"))
		if err != nil {
			lastID = 2147483646
		}
	} else {
		lastID = 2147483646
	}
	var sqlStatement string
	/*
			https://dba.stackexchange.com/questions/260817/postgresql-filtering-on-array-and-ordering-produces-wrong-plan-bad-index-choi
		  order by hack to increase query speed
	*/

	var orderBy = "(id + 0)"
	if address == "con_rocketswap_official_v1_1" {
		orderBy = "block_id"
	}
	// cannot replace block_id or (id+0) with orderBy variable coz the value will keep changing...
	if address == "con_rocketswap_official_v1_1" {
		if dir == "next" {
			sqlStatement = "SELECT hash, timestamp, id, contract, function, stamps_used, kwargs_amount, CASE sender WHEN $2 THEN 1 ELSE 0 END AS is_sender, has_error FROM transactions WHERE id < $1 AND addresses @> ARRAY[$2]::varchar[] ORDER BY block_id DESC LIMIT 25"
		} else {
			sqlStatement = `SELECT hash, timestamp, id, contract, function, stamps_used, kwargs_amount, is_sender, has_error FROM(SELECT hash, timestamp, id, contract, function, stamps_used, kwargs_amount, CASE sender WHEN $2 THEN 1 ELSE 0 END AS is_sender, has_error FROM transactions WHERE id > $1 AND ((addresses @> ARRAY[$2]::varchar[])) ORDER BY block_id ASC LIMIT 25) AS prev_page ORDER BY $3 DESC`
		}
	} else {
		if dir == "next" {
			sqlStatement = "SELECT hash, timestamp, id, contract, function, stamps_used, kwargs_amount, CASE sender WHEN $2 THEN 1 ELSE 0 END AS is_sender, has_error FROM transactions WHERE id < $1 AND addresses @> ARRAY[$2]::varchar[] ORDER BY (id + 0) DESC LIMIT 25"
		} else {
			sqlStatement = `SELECT hash, timestamp, id, contract, function, stamps_used, kwargs_amount, is_sender, has_error FROM(SELECT hash, timestamp, id, contract, function, stamps_used, kwargs_amount, CASE sender WHEN $2 THEN 1 ELSE 0 END AS is_sender, has_error FROM transactions WHERE id > $1 AND ((addresses @> ARRAY[$2]::varchar[])) ORDER BY (id + 0) ASC LIMIT 25) AS prev_page ORDER BY $3 DESC`
		}
	}

	fmt.Printf("statement lastID: %s %d\n", sqlStatement, lastID)

	//rows, err := db.Query(sqlStatement, address)
	rows, err := db.Query(sqlStatement, lastID, address)
	if err != nil {
		fmt.Printf("line 701\n")
	}
	fmt.Printf("order by: %s lastID: %d\n", orderBy, lastID)
	var transactions []TransactionDB
	for rows.Next() {

		var hash string

		var timestamp int
		var id int
		var contract string
		var function string
		var stampsUsed int
		var isSender int
		var hasError bool
		var ok int
		kwargsAmount := new(apd.Decimal)
		err = rows.Scan(&hash, &timestamp, &id, &contract, &function, &stampsUsed, &kwargsAmount, &isSender, &hasError)
		fmt.Printf("id %d : hash: %s\n", id, hash)
		var flow string
		if contract == "con_token_swap" && function == "disperse" && address == "con_token_swap" {
			flow = "out"
		} else {
			if isSender == 1 {
				flow = "out"
			} else {
				flow = "in"
			}
		}
		if hasError {
			ok = 0
		} else {
			ok = 1
		}
		transaction := TransactionDB{
			Hash:         hash,
			Timestamp:    timestamp,
			ID:           id,
			Contract:     contract,
			Function:     function,
			StampsUsed:   stampsUsed,
			KwargsAmount: kwargsAmount,
			Flow:         flow,
			OK:           ok,
		}
		transactions = append(transactions, transaction)

		if err != nil {
			fmt.Printf("line 744\n")
		}
	}

	fmt.Printf("txns: %+v\n", transactions)

	if address == "con_rocketswap_official_v1_1" {
		sqlStatement = `SELECT id FROM transactions WHERE addresses @> ARRAY[$1]::varchar[] ORDER BY block_id DESC LIMIT 1`
	} else {
		sqlStatement = `SELECT id FROM transactions WHERE addresses @> ARRAY[$1]::varchar[] ORDER BY (id+0) DESC LIMIT 1`
	}

	row := db.QueryRow(sqlStatement, address)
	var latestID int
	err = row.Scan(&latestID)
	if err != nil {
		fmt.Printf("line 753\n")
	}

	if address == "con_rocketswap_official_v1_1" {
		sqlStatement = `SELECT id FROM transactions WHERE addresses @> ARRAY[$1]::varchar[] ORDER BY block_id LIMIT 1`
	} else {
		sqlStatement = `SELECT id FROM transactions WHERE addresses @> ARRAY[$1]::varchar[] ORDER BY (id+0) LIMIT 1`
	}

	row = db.QueryRow(sqlStatement, address)
	var oldestID int
	err = row.Scan(&oldestID)
	if err != nil {
		fmt.Printf("line 761\n")
	}

	sqlStatement = `
	SELECT txn, balance, ROUND(balance * $1, 2) AS balance_usd, token_balance FROM top_addresses WHERE address = $2`
	balance := new(apd.Decimal)
	balanceUSD := new(apd.Decimal)
	txn := 0
	var tokenBalance string

	err = db.QueryRow(sqlStatement, tauPriceUSD, address).Scan(&txn, &balance, &balanceUSD, &tokenBalance)
	if err != nil {
		fmt.Printf("line 773\n")
	}
	var tokensDB []TokenDB
	if tokenBalance != "{}" && len(tokenBalance) > 2 {
		tokenBalance = strings.Replace(tokenBalance, "{", "", -1)
		tokenBalance = strings.Replace(tokenBalance, "}", "", -1)
		tokenBalance = strings.Replace(tokenBalance, "\"", "", -1)
		tokenBalance = strings.Replace(tokenBalance, " ", "", -1)
		tokens := strings.Split(tokenBalance, ",")
		for i := 0; i < len(tokens); i++ {
			tokenKV := strings.Split(tokens[i], ":")
			contractName := tokenKV[0]
			tokenName, err := redisClient.HGet("h_tau_token:"+contractName, "name").Result()
			if err != nil {
				fmt.Printf("line 787\n")
			}
			tokenSymbol, err := redisClient.HGet("h_tau_token:"+contractName, "symbol").Result()
			if err != nil {
				fmt.Printf("line 791\n")
			}
			tokensDB = append(tokensDB, TokenDB{TokenName: tokenName, TokenSymbol: tokenSymbol, TokenBalance: tokenKV[1], TokenContract: contractName})
		}
	}
	fmt.Printf("token b %s LEN %d\n", tokenBalance, len(tokenBalance))

	AddressTransaction := AddressTransactionDB{Transactions: transactions, LatestID: latestID, OldestID: oldestID, Txn: txn, Balance: balance, BalanceUSD: balanceUSD, Tokens: tokensDB}
	json.NewEncoder(w).Encode(AddressTransaction)
}

func getContracts(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	latestContractID, err := redisClient.Get("latest_contract_id").Result()
	if err != nil {
		fmt.Printf("has error: %s\n", err)
	}
	latestContractIDInt, err := strconv.Atoi(latestContractID)
	latestContract = latestContractIDInt
	page, err := strconv.Atoi(r.URL.Query().Get("page"))

	if err != nil || page < 2 {
		page = 1
	}
	var min = latestContract
	if page > 1 {
		min = latestContract - ((page - 1) * 25)
	}
	sqlStatement := `SELECT name, id FROM contracts WHERE id <= $1 ORDER BY id DESC LIMIT 25`

	rows, err := db.Query(sqlStatement, min)
	if err != nil {
		fmt.Printf("line 823\n")
	}
	var contracts []ContractSimpleDB
	for rows.Next() {
		var contractName string
		var contractID int
		err = rows.Scan(&contractName, &contractID)
		contract := ContractSimpleDB{
			ContractName: contractName,
			ID:           contractID,
		}
		contracts = append(contracts, contract)
		if err != nil {
			fmt.Printf("line 836\n")
		}
	}
	json.NewEncoder(w).Encode(contracts)
}

func getContract(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	vars := mux.Vars(r)
	id := sanitizeURL(vars["id"])
	sqlStatement := `SELECT name, contract_code, contract_creator, transaction_id, contract_created, byte_code FROM contracts WHERE name = $1`
	rows, err := db.Query(sqlStatement, id)
	if err != nil {
		fmt.Printf("line 849\n")
	}
	var contract ContractDB
	var name string
	var contractCode string
	var contractCreator string
	var txHash string
	var contractCreated int
	var byteCode string
	for rows.Next() {
		err = rows.Scan(&name, &contractCode, &contractCreator, &txHash, &contractCreated, &byteCode)
		if err != nil {
			fmt.Printf("line 861\n")
		}
	}
	contract = ContractDB{
		ContractName:    name,
		ContractCode:    contractCode,
		ContractCreator: contractCreator,
		TXHash:          txHash,
		ContractCreated: contractCreated,
		ByteCode:        byteCode,
	}
	json.NewEncoder(w).Encode(contract)
}

func getDapps(w http.ResponseWriter, r *http.Request) {
	sqlStatement := `SELECT name, id, category, description FROM dapps WHERE active = '1' ORDER BY RANDOM()`
	rows, err := db.Query(sqlStatement)
	if err != nil {
		fmt.Printf("line 879\n")
	}
	var dapps []DappDB
	for rows.Next() {
		var name string
		var dappID int
		var category string
		var description string
		err = rows.Scan(&name, &dappID, &category, &description)
		dapp := DappDB{
			Name:        name,
			DappID:      dappID,
			Category:    category,
			Description: description,
		}
		dapps = append(dapps, dapp)
		if err != nil {
			fmt.Printf("line 896\n")
		}
	}
	json.NewEncoder(w).Encode(dapps)
}

func getDapp(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	vars := mux.Vars(r)
	id := sanitizeURL(vars["id"])
	sqlStatement := `SELECT name, website, logo, main_contract_name, id, description, category FROM dapps WHERE REPLACE(LOWER(name), ' ', '_') = $1`
	rows, err := db.Query(sqlStatement, id)
	if err != nil {
		fmt.Printf("line 909\n")
	}
	var mainContract string
	var contract ContractDB
	var name string
	var website string
	var logo string
	var dappID int
	var dappDescription string
	var dappCategory string
	for rows.Next() {
		err = rows.Scan(&name, &website, &logo, &mainContract, &dappID, &dappDescription, &dappCategory)
		if err != nil {
			fmt.Printf("line 922\n")
		}
	}
	var contractList string
	if id == "lamden_bot" {
		contractList = "'con_dice001', 'con_multisend2'"
	} else if id == "rocketswap" {
		contractList = "'con_rocketswap_official_v1_1', 'con_simple_staking_tau_rswp_001'"
	} else {
		contractList = "'" + mainContract + "'"
	}

	sqlStatement = "SELECT to_char(to_timestamp(timestamp), 'YYYY-mm-dd') AS date, COUNT(*) AS txn, SUM(stamps_used) AS stamps_used, get_total_address(address_balance) AS total_unique_address from transactions WHERE contract IN (" + contractList + ") GROUP BY 1 ORDER BY 1"
	rows, err = db.Query(sqlStatement)
	if err != nil {
		fmt.Printf("line 937\n")
	}
	/*
		var arrayTxnMaster [][2]int
		var arrayStampsUsedMaster [][2]int
		var arrayAddressesMaster [][2]int
	*/

	var arrayTimestamp []int
	var arrayTxn []int
	var arrayStampsUsed []int
	var arrayAddresses []int

	for rows.Next() {
		var date string
		var txn int
		var stampsUsed int
		var addresses int
		err = rows.Scan(&date, &txn, &stampsUsed, &addresses)
		dateFormatted := fmt.Sprintf("%sT00:00:00+00:00", date)
		thetime, e := time.Parse(time.RFC3339, dateFormatted)
		if e != nil {
			fmt.Printf("line 959\n")
		}
		epoch := thetime.Unix()
		epoch *= 1000
		timestampFormatted := int(epoch)
		arrayTimestamp = append(arrayTimestamp, timestampFormatted)
		arrayTxn = append(arrayTxn, txn)
		arrayStampsUsed = append(arrayStampsUsed, stampsUsed)
		arrayAddresses = append(arrayAddresses, addresses)

	}

	var dailyTxn int
	var dailyStampsUsed int
	var dailyAddresses int
	var totalTxn int
	var totalStampsUsed int
	var totalAddresses int
	dailyTxnTmp, err := redisClient.HGet(mainContract, "daily_txn").Result()
	if err != nil {
		dailyTxn = 0
	} else {
		dailyTxn, err = strconv.Atoi(dailyTxnTmp)
		if err != nil {
			fmt.Printf("line 983\n")
		}
	}
	dailyStampsUsedTmp, err := redisClient.HGet(mainContract, "daily_stamps_used").Result()
	if err != nil {
		dailyStampsUsed = 0
	} else {
		dailyStampsUsed, err = strconv.Atoi(dailyStampsUsedTmp)
		if err != nil {
			fmt.Printf("line 992\n")
		}
	}
	dailyAddressesTmp, err := redisClient.HGet(mainContract, "daily_addresses").Result()
	if err != nil {
		dailyAddresses = 0
	} else {
		dailyAddresses, err = strconv.Atoi(dailyAddressesTmp)
		if err != nil {
			fmt.Printf("line 1001\n")
		}
	}
	totalTxnTmp, err := redisClient.HGet(mainContract, "total_txn").Result()
	if err != nil {
		totalTxn = 0
	} else {
		totalTxn, err = strconv.Atoi(totalTxnTmp)
		if err != nil {
			fmt.Printf("line 1010\n")
		}
	}
	totalStampsUsedTmp, err := redisClient.HGet(mainContract, "total_stamps_used").Result()
	if err != nil {
		totalStampsUsed = 0
	} else {
		totalStampsUsed, err = strconv.Atoi(totalStampsUsedTmp)
		if err != nil {
			fmt.Printf("line 1019\n")
		}
	}
	totalAddressesTmp, err := redisClient.HGet(mainContract, "total_addresses").Result()
	if err != nil {
		totalAddresses = 0
	} else {
		totalAddresses, err = strconv.Atoi(totalAddressesTmp)
		if err != nil {
			fmt.Printf("line 1028\n")
		}
	}

	var subcharts []SubChart
	subcharts = append(subcharts, SubChart{Name: "Txn", DataI: arrayTxn, Unit: "", Type: "column", Decimal: 0, Axis: "y"})
	subcharts = append(subcharts, SubChart{Name: "Stamps Used", DataI: arrayStampsUsed, Unit: "", Type: "line", Decimal: 0, Axis: "x"})
	subcharts = append(subcharts, SubChart{Name: "Users", DataI: arrayAddresses, Unit: "", Type: "line", Decimal: 0, Axis: "y"})
	mainChart := MainChart{XData: arrayTimestamp, Datasets: subcharts}

	contract = ContractDB{
		DailyTxn:        dailyTxn,
		DailyStampsUsed: dailyStampsUsed,
		DailyAddresses:  dailyAddresses,
		TotalTxn:        totalTxn,
		TotalStampsUsed: totalStampsUsed,
		TotalAddresses:  totalAddresses,
		ContractName:    mainContract,
		Website:         website,
		Logo:            logo,
		DappName:        name,
		ChartData:       mainChart,
		DappID:          dappID,
		DappDescription: dappDescription,
		DappCategory:    dappCategory,
	}

	json.NewEncoder(w).Encode(contract)
}

func getTokens(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	checkTAUPrice()
	dir := r.URL.Query().Get("dir")
	if dir != "prev" && dir != "next" {
		dir = "next"
	}
	var lastID int
	var lastMarketcap int64

	if len(r.URL.Query().Get("id")) > 0 {
		var err error
		lastID, err = strconv.Atoi(r.URL.Query().Get("id"))
		if err != nil {
			lastID = 2147483646
		}
	} else {
		lastID = 2147483646
	}
	if len(r.URL.Query().Get("balance")) > 0 {
		var err error
		lastMarketcap, err = strconv.ParseInt(r.URL.Query().Get("marketcap"), 10, 64)
		if err != nil {
			lastMarketcap = 2147483646
		}
	} else {
		lastMarketcap = 2147483646
	}

	var sqlStatement string
	if dir == "next" {
		sqlStatement = `SELECT id, name, token_name, token_symbol, marketcap_tau FROM contracts WHERE
	(marketcap_tau, id) < ($1, $2) AND is_token = '1' AND active = '1' ORDER BY marketcap_tau DESC, id DESC LIMIT 500`
	} else {
		sqlStatement = `SELECT id, name, token_name, token_symbol, marketcap_tau FROM(SELECT id, name, token_name, token_symbol, marketcap_tau FROM contracts WHERE (marketcap_tau, id) > ($1, $2) AND is_token = '1' AND active = '1' ORDER BY marketcap_tau ASC, id ASC LIMIT 500) AS prev_page ORDER BY marketcap_tau DESC, id DESC`
	}
	rows, err := db.Query(sqlStatement, lastMarketcap, lastID)
	if err != nil {
		fmt.Printf("line 1096\n")
	}
	var tokens []TokenDB
	for rows.Next() {
		var id uint64
		var tokenName string
		var tokenSymbol string
		var name string
		var marketcapTAU int
		err = rows.Scan(&id, &name, &tokenName, &tokenSymbol, &marketcapTAU)
		if err != nil {
			fmt.Printf("line 1107\n")
		}

		tokenIndividual := TokenDB{
			ID:            id,
			TokenName:     tokenName,
			TokenSymbol:   tokenSymbol,
			TokenContract: name,
			MarketcapTAU:  marketcapTAU,
		}
		tokens = append(tokens, tokenIndividual)

	}
	json.NewEncoder(w).Encode(tokens)
}

func getToken(w http.ResponseWriter, r *http.Request) {
	checkTAUPrice()
	fmt.Printf("url>>>: %s\n", r.URL.RequestURI())
	vars := mux.Vars(r)
	id := sanitizeURL(vars["id"])
	sqlStatement := `SELECT name, token_name, token_symbol, total_supply, social FROM contracts WHERE name = $1 AND is_token = '1' AND active = '1'`
	rows, err := db.Query(sqlStatement, id)
	if err != nil {
		fmt.Printf("line 1131\n")
	}

	tokenPrice, err := redisClient.HGet("h_tau_token:"+id, "last_price_tau").Result()
	if err != nil {
		fmt.Printf("line 1140\n")
	}
	lastPriceTAU, err := strconv.ParseFloat(tokenPrice, 64)
	if err != nil {
		fmt.Printf("line 1145\n")
	}

	var name string
	var tokenName string
	var tokenSymbol string
	var totalSupply uint64
	var circulatingSupply string
	var marketcapTAU int
	var social string

	for rows.Next() {
		err = rows.Scan(&name, &tokenName, &tokenSymbol, &totalSupply, &social)
		if err != nil {
			fmt.Printf("line 1160\n")
		}
	}

	sqlStatement = `SELECT COUNT(id) FROM top_addresses WHERE token_balance->>$1 is not null`
	row := db.QueryRow(sqlStatement, id)
	var tokenHolder int
	err = row.Scan(&tokenHolder)
	if err != nil {
		tokenHolder = 0
	}

	txn := 0
	sqlStatement = `SELECT COUNT(id) AS txcount FROM transactions WHERE ((contract = 'con_rocketswap_official_v1_1' AND kwargs->>'contract' = $1 AND function IN ('buy', 'sell')) OR 
(contract = $1 AND function = 'transfer') OR (contract = 'con_multisend2' AND kwargs->>'contract' = $1))`
	rows, err = db.Query(sqlStatement, id)
	if err != nil {
		fmt.Printf("line 1171\n")
	}
	for rows.Next() {
		var txCount int
		err = rows.Scan(&txCount)
		if err != nil {
			fmt.Printf("line 1177\n")
		}
		txn += txCount
	}
	var tokenBurned float64
	var lockedSupply float64
	var tokenPriceUSD string
	fmt.Printf("line 1174: %s\n", tauPriceUSD)
	if id == "con_gold_contract" || id == "con_demoncoin" || id == "con_lambden_contract" || id == "con_beer_contract" || id == "con_omicron_1" || id == "con_rswp_lst001" {
		burnedAddr := ""
		if id == "con_gold_contract" {
			burnedAddr = "0000000000000BURN0000000000000"
		} else if id == "con_demoncoin" {
			burnedAddr = "000000000000000000000000000000HELL000000000000000000000000000000"
		} else if id == "con_lambden_contract" {
			burnedAddr = "000000000000000000000000000000000000000000000000000000000000dead"
		} else if id == "con_omicron_1" {
			burnedAddr = "000Omicron00000000000000000000000000000000000000000000000Omicron"
		} else if id == "con_beer_contract" {
			burnedAddr = "0000000000000BURN0000000000000"
		} else if id == "con_rswp_lst001" {
			burnedAddr = "burn"

		}
		var row *sql.Row

		if id == "con_gold_contract" {
			sqlStatement = `SELECT SUM(CAST(token_balance->>$1 AS DOUBLE PRECISION)) FROM top_addresses WHERE address IN ('0000000000000BURN0000000000000', 'NEBULA_BURN_ADDRESS')`
			row = db.QueryRow(sqlStatement, id)
		} else {
			sqlStatement = `SELECT CAST(token_balance->>$1 AS DOUBLE PRECISION) FROM top_addresses WHERE address = $2`
			row = db.QueryRow(sqlStatement, id, burnedAddr)
		}
		err = row.Scan(&tokenBurned)
		if err != nil {
			fmt.Printf("line 1196\n")
			tokenBurned = 0
		}
		finalSupply := float64(totalSupply) - tokenBurned

		if id == "con_rswp_lst001" {
			sqlStatementLocked := `SELECT SUM((token_balance->>'con_rswp_lst001')::float) AS locked_supply FROM top_addresses WHERE token_balance->'con_rswp_lst001' IS NOT NULL AND address IN ('con_liq_mining_rswp_rswp', 'fcefe7743fa70c97ae2d5290fd673070da4b0293da095f0ae8aceccf5e62b6a1')`
			rowLocked := db.QueryRow(sqlStatementLocked)
			errLocked := rowLocked.Scan(&lockedSupply)
			if errLocked != nil {
				//nothing
			}
			circulatingSupply = fmt.Sprintf("%f", finalSupply-lockedSupply)
		} else {
			//circulatingSupply = finalSupply
			circulatingSupply = fmt.Sprintf("%f", finalSupply)
		}

		fmt.Printf("line 1191: %s\n", tauPriceUSD)
		tokenPriceUSD = tauPriceUSD
	} else if id == "con_nebula" {
		sqlStatement = `SELECT SUM((token_balance->>'con_nebula')::float) AS circulating_supply FROM top_addresses WHERE token_balance->'con_nebula' IS NOT NULL`
		row := db.QueryRow(sqlStatement)
		err = row.Scan(&circulatingSupply)
		if err != nil {
			//nothing
		}
		tokenPriceUSD = tauPriceUSD
	} else if id == "con_pusd_v1_2" {
		sqlStatement = `SELECT SUM((token_balance->>'con_pusd_v1_2')::float) AS circulating_supply FROM top_addresses WHERE token_balance->'con_pusd_v1_2' IS NOT NULL`
		row := db.QueryRow(sqlStatement)
		err = row.Scan(&circulatingSupply)
		if err != nil {
			//nothing
		}
		tokenPriceUSD = tauPriceUSD
	} else if id == "con_liquidus_lst001" {
		sqlStatement = `SELECT SUM((token_balance->>'con_liquidus_lst001')::float) AS circulating_supply FROM top_addresses WHERE token_balance->'con_liquidus_lst001' IS NOT NULL`
		row := db.QueryRow(sqlStatement)
		err = row.Scan(&circulatingSupply)
		if err != nil {
			//nothing
		}
		tokenPriceUSD = tauPriceUSD
	} else if id == "con_lusd_lst001" {
		sqlStatement = `SELECT SUM((token_balance->>'con_lusd_lst001')::float) AS circulating_supply FROM top_addresses WHERE token_balance->'con_lusd_lst001' IS NOT NULL`
		row := db.QueryRow(sqlStatement)
		err = row.Scan(&circulatingSupply)
		if err != nil {
			//nothing
		}
		tokenPriceUSD = tauPriceUSD
	} else if id == "con_mintorburn" {
		sqlStatement = `SELECT 10000000000 - (token_balance->>'con_mintorburn')::float AS circulating_supply FROM top_addresses WHERE address = 'None'`
		row := db.QueryRow(sqlStatement)
		err = row.Scan(&circulatingSupply)
		if err != nil {
			//nothing
		}
		tokenPriceUSD = tauPriceUSD
	} else if id == "con_bdt_lst001" {
		sqlStatement = `SELECT SUM((token_balance->>'con_bdt_lst001')::float) AS circulating_supply FROM top_addresses WHERE token_balance->'con_bdt_lst001' IS NOT NULL`
		row := db.QueryRow(sqlStatement)
		err = row.Scan(&circulatingSupply)
		if err != nil {
			//nothing
		}
		tokenPriceUSD = tauPriceUSD
	} else if id == "con_spange" {
		sqlStatement = `SELECT SUM((token_balance->>'con_spange')::float) AS circulating_supply FROM top_addresses WHERE token_balance->'con_spange' IS NOT NULL AND address != 'con_spange_jar'`
		row := db.QueryRow(sqlStatement)
		err = row.Scan(&circulatingSupply)
		if err != nil {
			//nothing
		}
		tokenPriceUSD = tauPriceUSD
	} else if id == "con_reflecttau_v2" {
		sqlStatement = `SELECT SUM((token_balance->>'con_reflecttau_v2')::float) AS circulating_supply FROM top_addresses WHERE token_balance->'con_reflecttau_v2' IS NOT NULL AND address NOT IN ('con_reflecttau_v2_reflection', 'reflecttau_burn_address')`
		row := db.QueryRow(sqlStatement)
		err = row.Scan(&circulatingSupply)
		if err != nil {
			//nothing
		}
		tokenPriceUSD = tauPriceUSD

		burnedAddr := "reflecttau_burn_address"
		sqlStatement2 := `SELECT CAST(token_balance->>$1 AS DOUBLE PRECISION) FROM top_addresses WHERE address = $2`
		row2 := db.QueryRow(sqlStatement2, id, burnedAddr)
		err = row2.Scan(&tokenBurned)
		if err != nil {
			fmt.Printf("line 1331\n")
			tokenBurned = 0
		}

	} else if id == "con_weth_lst001" {
		sqlStatement = `SELECT SUM((token_balance->>'con_weth_lst001')::float) AS circulating_supply FROM top_addresses WHERE token_balance->'con_weth_lst001' IS NOT NULL`
		row := db.QueryRow(sqlStatement)
		err = row.Scan(&circulatingSupply)
		if err != nil {
			//nothing
		}

		res, err := http.Get("https://api.coinpaprika.com/v1/ticker/weth-weth")
		if err != nil {
			fmt.Printf("line 1214\n")
		}
		body, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			fmt.Printf("line 1219\n")
		}
		if res.StatusCode != 200 {
			fmt.Printf("line 1222\n")
		}
		var wethTicker CoinPaprikaWethTicker
		err = json.Unmarshal(body, &wethTicker)
		if err != nil {
			fmt.Printf("line 1227\n")
		}
		// temporary: store WETH price in tauPriceUSD, maybe we will create a new attribute called token USD in the future
		tokenPriceUSD = wethTicker.PriceUSD
	} else {
		tokenPriceUSD = tauPriceUSD
	}

	fmt.Printf("tauprice: %s\n", tokenPriceUSD)
	token := TokenDB{
		TokenContract:     name,
		TokenName:         tokenName,
		TokenSymbol:       tokenSymbol,
		TotalSupply:       totalSupply,
		CirculatingSupply: circulatingSupply,
		MarketcapTAU:      marketcapTAU,
		LastPriceTAU:      lastPriceTAU,
		TokenHolder:       tokenHolder,
		Txn:               txn,
		TAUPriceUSD:       tokenPriceUSD,
		TokenBurned:       tokenBurned,
		Social:            json.RawMessage(social),
	}

	json.NewEncoder(w).Encode(token)
}

func getTokenAddresses(w http.ResponseWriter, r *http.Request) {
	checkTAUPrice()
	fmt.Printf("url: %s\n", r.URL.RequestURI())

	vars := mux.Vars(r)
	tokenName := sanitizeURL(vars["id"])
	dir := r.URL.Query().Get("dir")
	if dir != "prev" && dir != "next" {
		dir = "next"
	}
	var lastID int
	var lastBalance float64
	if len(r.URL.Query().Get("id")) > 0 {
		var err error
		lastID, err = strconv.Atoi(r.URL.Query().Get("id"))
		if err != nil {
			lastID = 2147483646
		}
	} else {
		lastID = 2147483646
	}
	if len(r.URL.Query().Get("balance")) > 0 {
		var err error
		lastBalance, err = strconv.ParseFloat(r.URL.Query().Get("balance"), 64)
		if err != nil {
			lastBalance = 9000000000000000
		}
	} else {
		lastBalance = 9000000000000000
	}

	tokenPrice, err := redisClient.HGet("h_tau_token:"+tokenName, "last_price_tau").Result()
	if err != nil {
		fmt.Printf("1294\n")
	}
	lastPriceTAU, err := strconv.ParseFloat(tokenPrice, 64)
	if err != nil {
		fmt.Printf("1299\n")
	}

	var sqlStatement string
	if dir == "next" {
		sqlStatement = `SELECT address, id, token_balance->$4 AS balance, ROUND((token_balance->$4)::numeric * $3 * $5, 2) AS balance_usd FROM top_addresses WHERE ((token_balance->$4)::numeric, id) < ($1, $2) AND token_balance->$4 IS NOT NULL ORDER BY token_balance->$4 DESC, id DESC LIMIT 25`
	} else {
		sqlStatement = `SELECT address, id, balance, balance_usd FROM(SELECT address, id, token_balance->$4 AS balance, ROUND((token_balance->$4)::numeric * $3 * $5, 2) AS balance_usd FROM top_addresses WHERE ((token_balance->$4)::numeric, id) > ($1, $2) AND token_balance->$4 IS NOT NULL ORDER BY balance ASC, id ASC LIMIT 25) AS prev_page ORDER BY balance DESC, id DESC`
	}
	rows, err := db.Query(sqlStatement, lastBalance, lastID, tauPriceUSD, tokenName, lastPriceTAU)
	if err != nil {
		fmt.Printf("1309\n")
	}
	var addresses []AddressDB
	for rows.Next() {
		var address string
		var id uint64
		var balance string
		var balanceUSD string
		err = rows.Scan(&address, &id, &balance, &balanceUSD)
		if err != nil {
			fmt.Printf("1319\n")
		}
		addressIndividual := AddressDB{
			Address:    address,
			ID:         id,
			Balance:    balance,
			BalanceUSD: balanceUSD,
		}
		addresses = append(addresses, addressIndividual)
	}
	totalSupply, err := redisClient.HGet("h_tau_token:"+tokenName, "total_supply").Result()
	if err != nil {
		fmt.Printf("1331\n")
	}
	tokenAddress := TokenAddressDB{addresses, totalSupply}
	json.NewEncoder(w).Encode(tokenAddress)
}

func getTokenTransfers(w http.ResponseWriter, r *http.Request) {
	checkTAUPrice()
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	vars := mux.Vars(r)
	tokenName := sanitizeURL(vars["id"])
	dir := r.URL.Query().Get("dir")
	if dir != "prev" && dir != "next" {
		dir = "next"
	}
	var lastID int
	if len(r.URL.Query().Get("id")) > 0 {
		var err error
		lastID, err = strconv.Atoi(r.URL.Query().Get("id"))
		if err != nil {
			lastID = 2147483646
		}
	} else {
		lastID = 2147483646
	}
	var sqlStatement string
	var querySender1 string
	var querySender2 string
	address := r.URL.Query().Get("address")
	if len(address) > 0 {
		querySender1 = "AND (sender = '" + address + "' OR kwargs->>'to' = '" + address + "')"
		querySender2 = "AND sender = '" + address + "'"

	} else {
		querySender1 = " "
		querySender2 = " "
	}

	if dir == "next" {
		sqlStatement = "SELECT hash, timestamp, id, sender, COALESCE(kwargs->>'to', '-') AS recipient, COALESCE(kwargs->'amount'->>'__fixed__', COALESCE(kwargs->>'amount', COALESCE(kwargs->'currency_amount'->>'__fixed__', COALESCE(kwargs->'token_amount'->>'__fixed__', '-')))) AS amount, has_error, contract, function FROM transactions WHERE ((contract = 'con_multisend2' AND kwargs->>'contract' = $1) OR (contract = $1 AND function = 'transfer' " + querySender1 + ") OR (contract = 'con_rocketswap_official_v1_1' AND kwargs->>'contract' = $1 AND function IN ('buy', 'sell') " + querySender2 + ")) AND id < $2 ORDER BY id DESC LIMIT 25"

	} else {
		sqlStatement = "SELECT hash, timestamp, id, sender, recipient, amount, has_error, contract, function FROM(SELECT hash, timestamp, id, sender, COALESCE(kwargs->>'to', '-') AS recipient, COALESCE(kwargs->'amount'->>'__fixed__', COALESCE(kwargs->>'amount', COALESCE(kwargs->'currency_amount'->>'__fixed__', COALESCE(kwargs->'token_amount'->>'__fixed__', '-')))) AS amount, has_error, function FROM transactions WHERE ((contract = 'con_multisend2' AND kwargs->>'contract' = $1) OR (contract = $1 AND function = 'transfer' " + querySender1 + ") OR (contract = 'con_rocketswap_official_v1_1' AND kwargs->>'contract' = $1 AND function IN ('buy', 'sell') " + querySender2 + ")) AND id > $2  " + querySender1 + " ORDER BY id ASC LIMIT 25) AS prev_page ORDER BY id DESC"
	}
	rows, err := db.Query(sqlStatement, tokenName, lastID)

	if err != nil {
		fmt.Printf("line 1378\n")
	}

	var transactions []TransactionDB
	for rows.Next() {
		var hash string
		var timestamp int
		var id int
		var sender string
		var recipient string
		var amount string
		var hasError bool
		var ok int
		var contract string
		var function string
		err = rows.Scan(&hash, &timestamp, &id, &sender, &recipient, &amount, &hasError, &contract, &function)
		if hasError {
			ok = 0
		} else {
			ok = 1
		}
		transaction := TransactionDB{
			Hash:      hash,
			Timestamp: timestamp,
			ID:        id,
			Sender:    sender,
			Recipient: recipient,
			Amount:    amount,
			OK:        ok,
			Contract:  contract,
			Function:  function,
		}
		transactions = append(transactions, transaction)
		if err != nil {
			fmt.Printf("line 1412\n")
		}
	}

	sqlStatement = `SELECT id FROM transactions WHERE contract = $1 AND function IN ('transfer') ORDER BY id DESC LIMIT 1`
	row := db.QueryRow(sqlStatement, tokenName)
	var latestID int
	err = row.Scan(&latestID)
	if err != nil {
		fmt.Printf("line 1421\n")
	}

	sqlStatement = `SELECT id FROM transactions WHERE contract = $1 AND function IN ('transfer') ORDER BY id LIMIT 1`
	row = db.QueryRow(sqlStatement, tokenName)
	var oldestID int
	err = row.Scan(&oldestID)
	if err != nil {
		fmt.Printf("line 1429\n")
	}

	tokenPrice, err := redisClient.HGet("h_tau_token:"+tokenName, "last_price_tau").Result()
	if err != nil {
		fmt.Printf("1431\n")
	}
	lastTokenPriceTAU, err := strconv.ParseFloat(tokenPrice, 64)
	if err != nil {
		fmt.Printf("1435\n")
	}

	var AddressTransaction AddressTransactionDB
	if len(address) > 0 {
		sqlStatement = `SELECT token_balance->$1 AS balance, ROUND((token_balance->$1)::numeric * $2 * $3, 2) AS balance_usd FROM top_addresses WHERE address =  $4`
		row = db.QueryRow(sqlStatement, tokenName, tauPriceUSD, lastTokenPriceTAU, address)
		var tokenBalance string
		var tokenBalanceUSD string
		err = row.Scan(&tokenBalance, &tokenBalanceUSD)
		if err != nil {
			fmt.Printf("line 1439\n")
		}

		totalSupply, err := redisClient.HGet("h_tau_token:"+tokenName, "total_supply").Result()
		if err != nil {
			fmt.Printf("line 1444\n")
		}
		tokenSymbol, err := redisClient.HGet("h_tau_token:"+tokenName, "symbol").Result()
		if err != nil {
			fmt.Printf("line 1448\n")
		}
		AddressTransaction = AddressTransactionDB{Transactions: transactions, LatestID: latestID, OldestID: oldestID, TokenBalance: tokenBalance, TokenBalanceUSD: tokenBalanceUSD, TotalSupply: totalSupply, TokenSymbol: tokenSymbol}
	} else {
		AddressTransaction = AddressTransactionDB{Transactions: transactions, LatestID: latestID, OldestID: oldestID}
	}
	json.NewEncoder(w).Encode(AddressTransaction)
}

func getTokenDextrades(w http.ResponseWriter, r *http.Request) {
	checkTAUPrice()
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	vars := mux.Vars(r)
	tokenName := sanitizeURL(vars["id"])
	dir := r.URL.Query().Get("dir")
	if dir != "prev" && dir != "next" {
		dir = "next"
	}
	var lastID int
	if len(r.URL.Query().Get("id")) > 0 {
		var err error
		lastID, err = strconv.Atoi(r.URL.Query().Get("id"))
		if err != nil {
			lastID = 2147483646
		}
	} else {
		lastID = 2147483646
	}

	var sqlStatement string
	var querySender1 string
	address := r.URL.Query().Get("address")
	if len(address) > 0 {
		querySender1 = "AND sender = '" + address + "'"

	} else {
		querySender1 = " "
	}

	if dir == "next" {
		sqlStatement = "SELECT hash, timestamp, id, function, CASE WHEN function = 'buy' THEN COALESCE(kwargs->'currency_amount'->>'__fixed__' , kwargs->>'currency_amount') ELSE COALESCE(kwargs->'token_amount'->>'__fixed__' , kwargs->>'token_amount') END AS amount, state, has_error FROM transactions WHERE contract = 'con_rocketswap_official_v1_1' AND function IN ('buy', 'sell') AND id < $2 AND kwargs->>'contract' = $1 " + querySender1 + " ORDER BY id DESC LIMIT 25"
	} else {
		sqlStatement = "SELECT hash, timestamp, id, function, amount, state, has_error FROM(SELECT hash, timestamp, id, function, CASE WHEN function = 'buy' THEN COALESCE(kwargs->'currency_amount'->>'__fixed__' , kwargs->>'currency_amount') ELSE COALESCE(kwargs->'token_amount'->>'__fixed__' , kwargs->>'token_amount') END AS amount, state, has_error FROM transactions WHERE contract = 'con_rocketswap_official_v1_1' AND function IN ('buy', 'sell') AND id > $2 AND kwargs->>'contract' = $1  " + querySender1 + " ORDER BY id ASC LIMIT 25) AS prev_page ORDER BY id DESC"
	}
	rows, err := db.Query(sqlStatement, tokenName, lastID)
	if err != nil {
		fmt.Printf("line 1494\n")
	}

	var transactions []TransactionDB
	for rows.Next() {
		var hash string
		var timestamp int
		var id int
		var function string
		var amount string
		var state string
		var swapRateTAU string
		var hasError bool
		var ok int
		err = rows.Scan(&hash, &timestamp, &id, &function, &amount, &state, &hasError)
		var states []State
		err = json.Unmarshal([]byte(state), &states)
		if err != nil {
			fmt.Printf("line 1512\n")
		}
		for i := range states {
			if states[i].Key == "con_rocketswap_official_v1_1.prices:"+tokenName {
				//convert rawMessage to string
				rVal, err := json.Marshal(&states[i].Value)
				if err != nil {
					fmt.Printf("line 1519\n")
				}
				valStr := string(rVal)
				valArr := strings.Split(valStr, "\"")
				swapRateTAU = valArr[3]

			}
		}
		if hasError {
			ok = 0
		} else {
			ok = 1
		}
		transaction := TransactionDB{
			Hash:        hash,
			Timestamp:   timestamp,
			ID:          id,
			Amount:      amount,
			SwapRateTAU: swapRateTAU,
			TAUPriceUSD: tauPriceUSD,
			Action:      function,
			OK:          ok,
		}
		transactions = append(transactions, transaction)
		if err != nil {
			fmt.Printf("line 1544\n")
		}
	}

	sqlStatement = `SELECT id FROM transactions WHERE contract = 'con_rocketswap_official_v1_1' AND function IN ('buy', 'sell') AND kwargs->>'contract' = $1 ORDER BY id DESC LIMIT 1`
	row := db.QueryRow(sqlStatement, tokenName)
	var latestID int
	err = row.Scan(&latestID)
	if err != nil {
		fmt.Printf("line 1553\n")
	}

	sqlStatement = `SELECT id FROM transactions WHERE contract = 'con_rocketswap_official_v1_1' AND function IN ('buy', 'sell') AND kwargs->>'contract' = $1 ORDER BY id LIMIT 1`
	row = db.QueryRow(sqlStatement, tokenName)
	var oldestID int
	err = row.Scan(&oldestID)
	if err != nil {
		fmt.Printf("line 1561\n")
	}
	symbol, err := redisClient.HGet("h_tau_token:"+tokenName, "symbol").Result()
	if err != nil {
		fmt.Printf("line 1565\n")
	}
	AddressTransaction := AddressTransactionDB{Transactions: transactions, LatestID: latestID, OldestID: oldestID, Symbol: symbol}
	json.NewEncoder(w).Encode(AddressTransaction)
}

func getTokenChart(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	vars := mux.Vars(r)
	tokenName := sanitizeURL(vars["id"])
	/*
		sqlStatement := `SELECT to_char(to_timestamp(timestamp), 'YYYY-mm-dd') AS date, AVG(tau_swap_rate::float) AS swap_rate, AVG(tau_swap_rate::float * tau_price_usd::float) AS token_price_usd, SUM(tx_value_usd::float) AS volume_usd FROM transactions WHERE contract = 'con_rocketswap_official_v1_1' AND kwargs->>'contract' = $1 AND function IN ('buy', 'sell') AND has_error is false AND id > 52000 GROUP BY 1 ORDER BY 1`
	*/
	sqlStatement := `SELECT timestamp AS date, tau_swap_rate::float AS swap_rate, (tau_swap_rate::float * tau_price_usd::float) AS token_price_usd, 
	tx_value_usd::float AS volume_usd FROM transactions WHERE contract = 'con_rocketswap_official_v1_1' AND kwargs->>'contract' = $1 AND 
	function IN ('buy', 'sell') AND has_error is false ORDER BY id`
	rows, err := db.Query(sqlStatement, tokenName)
	if err != nil {
		fmt.Printf("line 1583\n")
	}
	var arraySwapRate []float32
	var arrayTokenPriceUSD []float32
	var arrayVolumeUSD []float32
	var arrayTimestamp []int
	for rows.Next() {
		var date string
		var swapRate float32
		var tokenPriceUSD float32
		var volumeUSD float32
		err = rows.Scan(&date, &swapRate, &tokenPriceUSD, &volumeUSD)
		i, err := strconv.Atoi(date)
		if err != nil {
			fmt.Printf("line 1600\n")
		}
		timestampFormatted := i * 1000
		//fmt.Printf("swapRate: %s tokenPrice %s volume: %s txn: %s\n", swapRate, tokenPriceUSD, volumeUSD, txn)
		arraySwapRate = append(arraySwapRate, swapRate)
		arrayTokenPriceUSD = append(arrayTokenPriceUSD, tokenPriceUSD)
		arrayVolumeUSD = append(arrayVolumeUSD, volumeUSD)
		arrayTimestamp = append(arrayTimestamp, timestampFormatted)
	}
	var subcharts []SubChart
	subcharts = append(subcharts, SubChart{Name: "Swap Rate", DataF: arraySwapRate, Unit: "TAU", Type: "line", Decimal: 4, Axis: "x", Suffix: " TAU"})
	subcharts = append(subcharts, SubChart{Name: "Price", DataF: arrayTokenPriceUSD, Unit: "USD", Type: "line", Decimal: 4, Axis: "x", Prefix: "$"})
	subcharts = append(subcharts, SubChart{Name: "Volume", DataF: arrayVolumeUSD, Unit: "USD", Type: "column", Decimal: 0, Axis: "y", Prefix: "$"})
	tokenChart := TokenChartDB{MainChart{XData: arrayTimestamp, Datasets: subcharts}}
	json.NewEncoder(w).Encode(tokenChart)
}

func getStat(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	vars := mux.Vars(r)
	statType := vars["id"]
	var sqlStatement string
	if statType == "tau_lost_forever" {
		checkTAUPrice()
		sqlStatement = `SELECT address, balance, ROUND(balance * $1, 2) AS balance_usd FROM top_addresses WHERE first_tx_timestamp != 0 AND LENGTH(TRIM(address)) != 64 AND address NOT LIKE 'con_%' AND balance > 0 AND address NOT IN ('currency_2') OR address = ' 5b09493df6c18d17cc883ebce54fcb1f5afbd507533417fe32c006009a9c3c4a' ORDER BY balance DESC`
		rows, err := db.Query(sqlStatement, tauPriceUSD)
		if err != nil {
			fmt.Printf("line 1627\n")
		}
		var tauLost []AddressDB
		for rows.Next() {
			var address string
			var balance string
			var balanceUSD string
			err = rows.Scan(&address, &balance, &balanceUSD)
			tauLost = append(tauLost, AddressDB{
				Address:    address,
				Balance:    balance,
				BalanceUSD: balanceUSD,
			})
		}
		json.NewEncoder(w).Encode(tauLost)
	} else {

		var sqlStatement2 string
		var arrayDailyBurned []float32
		var arrayDailyCount []int
		var arrayDailyCount2 []int
		var arrayTimestamp []int
		var arrayTotal []int
		var subChartName string
		var subcharts []SubChart
		var normalQuery int
		if statType == "txns" {
			sqlStatement = `SELECT to_char(to_timestamp(timestamp), 'YYYY-mm-dd') AS date, COUNT(id) FROM transactions WHERE timestamp IS NOT NULL GROUP BY 1 ORDER BY 1`
			subChartName = "Txn"
			normalQuery = 1
		} else if statType == "burned" {
			sqlStatement = `SELECT to_char(to_timestamp(timestamp), 'YYYY-mm-dd') AS date, SUM((stamps_used*stamps_burn_ratio)/stamps_per_tau) FROM transactions WHERE timestamp IS NOT NULL GROUP BY 1 ORDER BY 1`
			subChartName = "Burned TAU"
			normalQuery = 2
		} else if statType == "addresses" {
			sqlStatement = `SELECT to_char(to_timestamp(first_tx_timestamp), 'YYYY-mm-dd') AS date, COUNT(id) FROM top_addresses WHERE first_tx_timestamp != 0 GROUP BY 1 ORDER BY 1`
			subChartName = "Wallet Created"
			normalQuery = 1
		} else if statType == "users" {
			//excluding block 71060
			sqlStatement = `SELECT to_char(to_timestamp(timestamp), 'YYYY-mm-dd') AS date, get_total_address(address_balance) FROM transactions WHERE id != 71060 GROUP BY 1 ORDER BY 1`
			subChartName = "Active Users"
			normalQuery = 1
		} else if statType == "txns_addresses" {
			sqlStatement = `SELECT to_char(to_timestamp(timestamp), 'YYYY-mm-dd') AS date, COUNT(id) FROM transactions WHERE timestamp IS NOT NULL GROUP BY 1 ORDER BY 1`
			sqlStatement2 = `SELECT to_char(to_timestamp(first_tx_timestamp), 'YYYY-mm-dd') AS date, COUNT(id) FROM top_addresses WHERE first_tx_timestamp != 0 GROUP BY 1 ORDER BY 1`
			normalQuery = 0
		}

		if normalQuery == 1 {
			rows, err := db.Query(sqlStatement)
			if err != nil {
				fmt.Printf("line 1674\n")
			}
			var total int
			for rows.Next() {
				var date string
				var dailyCount int
				err = rows.Scan(&date, &dailyCount)
				total += dailyCount
				dateFormatted := fmt.Sprintf("%sT00:00:00+00:00", date)
				thetime, e := time.Parse(time.RFC3339, dateFormatted)
				if e != nil {
					fmt.Printf("line 1685\n")
				}
				epoch := thetime.Unix()
				epoch *= 1000
				timestampFormatted := int(epoch)
				arrayDailyCount = append(arrayDailyCount, dailyCount)
				arrayTimestamp = append(arrayTimestamp, timestampFormatted)
				arrayTotal = append(arrayTotal, total)
			}

			if statType == "users" {
				subcharts = append(subcharts, SubChart{Name: subChartName, DataI: arrayDailyCount, Unit: "", Type: "column", Decimal: 0, Axis: "x"})
			} else {
				subcharts = append(subcharts, SubChart{Name: subChartName, DataI: arrayDailyCount, Unit: "", Type: "column", Decimal: 0, Axis: "y"})
				subcharts = append(subcharts, SubChart{Name: "Total", DataI: arrayTotal, Unit: "", Type: "line", Decimal: 0, Axis: "x"})
			}

		} else if normalQuery == 2 {
			rows, err := db.Query(sqlStatement)
			if err != nil {
				fmt.Printf("line 1743\n")
			}
			for rows.Next() {
				var date string
				var dailyBurned float32
				err = rows.Scan(&date, &dailyBurned)
				dateFormatted := fmt.Sprintf("%sT00:00:00+00:00", date)
				thetime, e := time.Parse(time.RFC3339, dateFormatted)
				if e != nil {
					fmt.Printf("line 1685\n")
				}
				epoch := thetime.Unix()
				epoch *= 1000
				timestampFormatted := int(epoch)
				arrayDailyBurned = append(arrayDailyBurned, dailyBurned)
				arrayTimestamp = append(arrayTimestamp, timestampFormatted)
			}

			subcharts = append(subcharts, SubChart{Name: subChartName, DataF: arrayDailyBurned, Unit: "", Type: "column", Decimal: 8, Axis: "x"})

		} else {
			rows, err := db.Query(sqlStatement)
			if err != nil {
				fmt.Printf("line 1705\n")
			}
			mapTimeStamp := make(map[int]int)
			for rows.Next() {
				var date string
				var dailyCount int
				err = rows.Scan(&date, &dailyCount)
				dateFormatted := fmt.Sprintf("%sT00:00:00+00:00", date)
				thetime, e := time.Parse(time.RFC3339, dateFormatted)
				if e != nil {
					fmt.Printf("line 1705\n")
				}
				epoch := thetime.Unix()
				epoch *= 1000
				timestampFormatted := int(epoch)
				arrayDailyCount = append(arrayDailyCount, dailyCount)
				arrayTimestamp = append(arrayTimestamp, timestampFormatted)
				mapTimeStamp[timestampFormatted] = 0
			}
			subcharts = append(subcharts, SubChart{Name: "Txn", DataI: arrayDailyCount, Unit: "", Type: "line", Decimal: 0, Axis: "x"})
			rows2, err := db.Query(sqlStatement2)
			if err != nil {
				fmt.Printf("line 1727\n")
			}
			for rows2.Next() {
				var date string
				var dailyCount int
				err = rows2.Scan(&date, &dailyCount)
				dateFormatted := fmt.Sprintf("%sT00:00:00+00:00", date)
				thetime, e := time.Parse(time.RFC3339, dateFormatted)
				if e != nil {
					fmt.Printf("line 1726\n")
				}
				epoch := thetime.Unix()
				epoch *= 1000
				timestampFormatted := int(epoch)
				mapTimeStamp[timestampFormatted] = dailyCount
			}

			//To store the keys in slice in sorted order
			//https://stackoverflow.com/questions/18695346/how-to-sort-a-mapstringint-by-its-values
			keys := make([]int, len(mapTimeStamp))
			i := 0
			for k := range mapTimeStamp {
				keys[i] = k
				i++
			}
			sort.Ints(keys)
			for _, k := range keys {
				arrayDailyCount2 = append(arrayDailyCount2, mapTimeStamp[k])
			}
			subcharts = append(subcharts, SubChart{Name: "Wallet Created", DataI: arrayDailyCount2, Unit: "", Type: "line", Decimal: 0, Axis: "y"})
		}
		tokenChart := TokenChartDB{MainChart{XData: arrayTimestamp, Datasets: subcharts}}
		json.NewEncoder(w).Encode(tokenChart)
	}
}

func getSearch(w http.ResponseWriter, r *http.Request) {
	fmt.Printf("url: %s\n", r.URL.RequestURI())
	keyword := sanitizeURL(r.URL.Query().Get("keyword"))

	sqlStatement := `SELECT count(id) FROM transactions WHERE hash = $1`
	rows := db.QueryRow(sqlStatement, keyword)
	var count int = 0
	var contractName = ""
	var searchType string = "none"
	err := rows.Scan(&count)
	if err != nil {
		fmt.Printf("select transactions error\n")
	}
	if count == 0 {
		sqlStatement = `SELECT count(id) FROM blocks WHERE id = $1`
		rows = db.QueryRow(sqlStatement, keyword)
		err := rows.Scan(&count)
		if err != nil {
			fmt.Printf("select blocks error\n")
		}
		if count == 0 {
			sqlStatement = `SELECT count(id) FROM top_addresses WHERE address = $1`
			rows = db.QueryRow(sqlStatement, keyword)
			err := rows.Scan(&count)
			if err != nil {
				fmt.Printf("select top_addresses error\n")
			}
			if count == 0 {
				sqlStatement = `SELECT name FROM contracts WHERE LOWER(token_name) = $1 OR LOWER(token_symbol) = $1`
				rows = db.QueryRow(sqlStatement, keyword)
				err := rows.Scan(&contractName)
				if err != nil {
					fmt.Printf("select contracts error\n")
				}
				if contractName == "" {
					sqlStatement = `SELECT count(id) FROM contracts WHERE name = $1`
					rows = db.QueryRow(sqlStatement, keyword)
					err := rows.Scan(&count)
					if err != nil {
						fmt.Printf("select contracts error\n")
					}
					if count > 0 {
						searchType = "/contracts/" + keyword
					} else {
						searchType = "notfound"
					}
				} else {
					searchType = "/tokens/" + contractName
				}
			} else {
				searchType = "/addresses/" + keyword
			}

		} else {
			searchType = "/blocks/" + keyword
		}
	} else {
		searchType = "/transactions/" + keyword
	}
	searchResult := SearchResult{Type: searchType}
	json.NewEncoder(w).Encode(searchResult)
}

func handleRequests() {
	fmt.Printf("handle requests\n")
	c := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},   // All origins
		AllowedMethods: []string{"GET"}, // Allowing only get, just an example
	})
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/summary", getSummary)
	myRouter.HandleFunc("/blocks", getBlocks)
	myRouter.HandleFunc("/blocks/{id}", getBlock)
	myRouter.HandleFunc("/transactions", getTransactions)
	myRouter.HandleFunc("/transactions/{id}", getTransaction)
	myRouter.HandleFunc("/addresses", getAddresses)
	myRouter.HandleFunc("/addresses/{address}", getAddress)
	myRouter.HandleFunc("/contracts", getContracts)
	myRouter.HandleFunc("/contracts/{id}", getContract)
	myRouter.HandleFunc("/dapps", getDapps)
	myRouter.HandleFunc("/dapps/{id}", getDapp)
	myRouter.HandleFunc("/tokens", getTokens)
	myRouter.HandleFunc("/tokens/{id}", getToken)
	myRouter.HandleFunc("/tokens/{id}/addresses", getTokenAddresses)
	myRouter.HandleFunc("/tokens/{id}/transfers", getTokenTransfers)
	myRouter.HandleFunc("/tokens/{id}/dextrades", getTokenDextrades)
	myRouter.HandleFunc("/tokens/{id}/chart", getTokenChart)
	myRouter.HandleFunc("/stats/{id}", getStat)

	myRouter.HandleFunc("/search", getSearch)

	log.Fatal(http.ListenAndServe(":10000", c.Handler(myRouter)))
}

func main() {

	fmt.Printf("line main()\n")

	//err := godotenv.Load(".env")
	err := godotenv.Load("/app/config/.env")
	if err != nil {
		log.Fatalf("Error loading .env file")
	}

	redisClient = redis.NewClient(&redis.Options{
		Addr:     os.Getenv("REDISHOST"),
		Password: os.Getenv("REDISPASS"),
		DB:       0,
	})

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s sslmode=disable",
		os.Getenv("DBHOST"), 5000, os.Getenv("DBUSER"), os.Getenv("DBPASS"), os.Getenv("DBNAME"))
	db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	fmt.Printf("redis %s %s\n", os.Getenv("REDISHOST"), os.Getenv("REDISPASS"))
	fmt.Printf("info: %s\n", psqlInfo)

	handleRequests()
}
