FROM golang:1.16-alpine as builder

WORKDIR /app

COPY . .

RUN go mod download
RUN go build -o /tauhq-api-server-arko

EXPOSE 10000


CMD [ "/tauhq-api-server-arko" ]

